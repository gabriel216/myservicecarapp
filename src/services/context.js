import React, { Component } from 'react'
import {  publicationsAvailables } from './data';


//create a new context object 
const ServiceContext = React.createContext();
// Contetx API comes with 2 components Provider, Consumer

class ServiceProvider extends Component {
    state = { 
        publications : [],
        publicationsFilter : [],
        detailPublication : {},
        services     : [],
        cart         : []
    };
    
    componentDidMount() {
        this.loadPublications();    
    }
    
    loadPublications = () => {
        this.setState({publications: publicationsAvailables, publicationsFilter: publicationsAvailables});
    }

    filterByType = (type) => {
        const publicationsFiltered = this.state.publications.filter(publication => publication.type === type);
        this.setState({ publicationsFilter: publicationsFiltered });
    }

    // get an publication based on id 
    getDetailPublication = (id) => {
        const publication  = this.state.publications.find(publication => publication.id ===id);
        return publication;
    }
    
    handleDetailPublication = (id) => {
        const publication = this.getDetailPublication(id);
        this.setState({ detailPublication: publication, services : publication.planes });
    }

    addServiceToCart = (id) => {
        let tempServices = [...this.state.services];
            tempServices = tempServices.find(service => service.checked);
        const index = tempServices.indexOf(this.getItem(id)); 
        const service = tempServices[index];
        service.inCart = true;
        // change the values in the state
        this.setState({ services: tempServices, cart : [...this.state.cart, service] });
    }

    clearCart = () => {
        this.setState(() => {
            return {cart: [] } 
        });
    }

    render() {
        return (
            <ServiceContext.Provider value={{
                // use destructuring here
                ...this.state,
                handleDetailPublication: this.handleDetailPublication,
                getDetailPublication   : this.getDetailPublication,
                addServiceToCart: this.addServiceToCart,
                clearCart: this.clearCart,
                filterByType: this.filterByType,
            }}
            >
                {this.props.children}
            </ServiceContext.Provider>
        )
    }
}

const ServiceConsumer = ServiceContext.Consumer;

export { ServiceProvider, ServiceConsumer };