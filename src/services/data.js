  export const headers =  {
    headers : { 
      'Content-Type': 'application/json'
     }
  };

  /*
  export interface IClient {
    nombre : String;
    correo : String;
    numero : String;
  }

  export interface IContract {
     id : Number;
     client : IClient;
     services : Array<IService>;
     fechaServicio : Date;
     total : Number;
     state : 'en proceso' | 'contratado' | 'en carro';
  }

  export interface ICompany {
    nombre : String;
    rubro  : String;
    logo   : String;
  }

  export interface IService {
    id          : Number;
    titulo      : String;
    descripcion : String;
    stock       : Number;
    valor       : String;
    modalidad   : 'domicilio' | 'local'
  }
  

  export interface IPublication {
    id     : Number;
    title ?: String;
    images ?: Array<String>;
    description ?: String;
    fechaSubida ?: Date;
    company ?: ICompany;
    planes  ?: Array<IService>;
  } */

  export const publicationsAvailables = [
      {
        id    : 220483749173,
        title: 'Servicio de lavado de autos',
        type: 'Servicio de autolavado',
        images : [require('../../assets/images/carWash.jpg'),require('../../assets/images/carWash.jpg')],
        description : ' Servicio enfocado en el lavado integral de su automóvil. Contamos con cobertura a toda la región metropolitana.',
        fechaSubida : new Date (),
        company     : {
            nombre: 'Car Wash S.A.',
            logo  : '../../assets/images/carWash.jpg',
            rubro : 'Lavado automotriz'
        },
        planes  : [
          {
            id : 78394792374,
            titulo : 'Lavado exterior básico',
            descripcion : 'Lavado a base de shampoo automovil kit',
            valor       : 3000,
            modalidad   : 'local'
          },
          {
            id : 3234124134,
            titulo : 'Lavado exterior básico',
            descripcion : 'Lavado a base de shampoo automovil kit',
            valor       : 4000,
            modalidad   : 'domicilio'
          },
          {
            id : 28276381873,
            titulo : 'Lavado exterior + interior full',
            descripcion : 'Lavado a base de shampoo automovil kit + cera pulir + limpieza tapiz',
            valor       : 17000,
            modalidad   : 'local'
          },
          {
            id : 7568474839,
            titulo : 'Lavado exterior + interior full',
            descripcion : 'Lavado a base de shampoo automovil kit + cera pulir + limpieza tapiz',
            valor       : 20000,
            modalidad   : 'domicilio'
          }
        ]
      },
      {
        id    : 59237234618,
        title: 'Servicio de mecánica',
        type: 'Servicio de mecánica',
        images : [require('../../assets/images/mecanica.jpg'),require('../../assets/images/carWash.jpg')],
        description : 'Servicio de mecánica etc. Atendemos las 24 horas',
        fechaSubida : new Date (),
        company     : {
            nombre: 'Key value S.A.',
            logo  : require('../../assets/images/carWash.jpg'),
            rubro : 'Hogar'
        },
        planes  : [
          {
            id     : 2341351341,
            titulo : 'Apertura de chapa',
            descripcion : 'Apertura básica de puerta de ingreso a hogar.',
            valor       : 5000,
            modalidad   : 'domicilio'
          },
          {
            id     : 938332342,
            titulo : 'Cambio de chapa',
            descripcion : 'Cambio de chapa de puerta ingreso / reja u otro acceso (previa evaluación',
            valor       : 15000,
            modalidad   : 'domicilio'
          },
          {
            id : 281612938,
            titulo : 'Copia de llave',
            descripcion : 'Generación de copia de llave de acceso directamente en su domicilio. (en caso de extravío o rotura de la original).',
            valor       : 6000,
            modalidad   : 'domicilio'
          }
        ]
      },
      {
        id    : 592372346189128912,
        title: 'Servicio de autos KIA',
        type: 'Servicio de mecánica',
        images : [require('../../assets/images/kia.png'),require('../../assets/images/carWash.jpg')],
        description : ' Servicio de mecánica. Atendemos las 24 horas',
        fechaSubida : new Date (),
        company     : {
            nombre: 'KIA MOTORS S.A.',
            logo  : require('../../assets/images/carWash.jpg'),
            rubro : 'Hogar'
        },
        planes  : [
          {
            id     : 2341351341,
            titulo : 'Apertura de chapa',
            descripcion : 'Apertura básica de puerta de ingreso a hogar.',
            valor       : 5000,
            modalidad   : 'domicilio'
          },
          {
            id     : 938332342,
            titulo : 'Cambio de chapa',
            descripcion : 'Cambio de chapa de puerta ingreso / reja u otro acceso (previa evaluación',
            valor       : 15000,
            modalidad   : 'domicilio'
          },
          {
            id : 281612938,
            titulo : 'Copia de llave',
            descripcion : 'Generación de copia de llave de acceso directamente en su domicilio. (en caso de extravío o rotura de la original).',
            valor       : 6000,
            modalidad   : 'domicilio'
          }
        ]
      },
      {
        id    : 592372346140,
        title: 'Servicio de grúa',
        type: 'Servicio grúa',
        images : [require('../../assets/images/grua.jpg'),require('../../assets/images/grua.jpg')],
        description : ' Servicio totalmente confiable y eficaz. Atendemos las 24 horas',
        fechaSubida : new Date (),
        company     : {
            nombre: 'Grua S.A.',
            logo  : require('../../assets/images/carWash.jpg'),
            rubro : 'Hogar'
        },
        planes  : [
          {
            id     : 23413554341,
            titulo : 'Apertura de chapa',
            descripcion : 'Apertura básica de puerta de ingreso a hogar.',
            valor       : 5000,
            modalidad   : 'domicilio'
          },
          {
            id     : 93833244342,
            titulo : 'Cambio de chapa',
            descripcion : 'Cambio de chapa de puerta ingreso / reja u otro acceso (previa evaluación',
            valor       : 15000,
            modalidad   : 'domicilio'
          },
          {
            id : 28161442938,
            titulo : 'Copia de llave',
            descripcion : 'Generación de copia de llave de acceso directamente en su domicilio. (en caso de extravío o rotura de la original).',
            valor       : 6000,
            modalidad   : 'domicilio'
          }
        ]
      },
  ];
  