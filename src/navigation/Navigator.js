import React, { Component } from 'react';
import { Dimensions } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';
import HomeScreen from '../screens/Home';
import SignIn from '../screens/SignIn';
import SignUp from '../screens/SignUp';
import MyAccount from '../screens/MyAccount';
import PublicationDetail from '../screens/PublicationDetail';
import Categories from '../screens/Categories';
import Category from '../screens/Category';
import CategoryDetail from '../screens/CategoryDetail';

const WIDTH = Dimensions.get('window').width;

const DrawerConfig = {
  drawerWidth: WIDTH * 0.83,
  drawerBackgroundColor: '#3B5998',
  contentOptions: {
    activeTintColor: '#fff',
    labelStyle: {
      color: 'white',
    },
  },
};

class Hidden extends Component {
  render() {
    return null;
  }
}
const AppDrawer = createDrawerNavigator(
  {
    SignIn: {
      screen: SignIn,
      navigationOptions: {
        drawerLabel: <Hidden />,
      }
    },
    SignUp: {
      screen: SignUp,
      navigationOptions: {
        drawerLabel: <Hidden />,
      }
    },
    Home: {
      screen: HomeScreen,
      navigationOptions: {
        title: 'Inicio'
      }
    },
    Categories: {
      screen: Categories,
      navigationOptions: {
        title: 'Categorias'
      }
    },
    Category: {
      screen: Category,
      navigationOptions: {
        drawerLabel: <Hidden />
      }
    },
    CategoryDetail: {
      screen: CategoryDetail,
      navigationOptions: {
        drawerLabel: <Hidden />
      }
    },
    MyAccount: {
      screen: MyAccount,
      navigationOptions: {
        title: 'Perfil'
      }
    },
    PublicationDetail: {
      screen: PublicationDetail,
      navigationOptions: {
        drawerLabel: <Hidden />
      }
    },
  },
  DrawerConfig,
);

export default createAppContainer(AppDrawer);
