import React, {Component} from 'react';
import {View, Text, StyleSheet, TextInput, Image, ScrollView } from 'react-native';
import HeaderMenu from '../components/MenuButton';
import AppButton from '../components/appButton';
import {Icon} from 'native-base';
import {Input} from 'react-native-elements';
import User from '../../assets/images/user.png';

class MyAccount extends Component {
  render() {
    const {navigation} = this.props;
    return (
      <ScrollView>
        <HeaderMenu navigation={navigation} />
        <View style={styles.container}>
          <View style={styles.containerImage}>
            <View style={styles.circleContainer}>
              <Image source={User} style={styles.user} />
            </View>
          </View>
          {/* Nombre  */}
          <Text style={styles.text}>Nombre</Text>
          <TextInput
            placeholder="Nombre"
            style={styles.input}
            leftIcon={
              <Icon
                name="name"
                size={24}
                color="black"
                style={{paddingRight: 7}}
              />
            }
          />
          {/* Apellido  */}
          <Text style={styles.text}>Apellido</Text>
          <TextInput
            placeholder="Apellido"
            style={styles.input}
            leftIcon={
              <Icon
                name="lastname"
                size={24}
                color="black"
                style={{paddingRight: 7}}
              />
            }
          />
          {/* Correo  */}
          <Text style={styles.text}>Correo electrónico</Text>
          <TextInput
            placeholder="Correo"
            style={styles.input}
            leftIcon={
              <Icon
                name="mail"
                size={24}
                color="black"
                style={{paddingRight: 7}}
              />
            }
          />
          <AppButton
            bgColor="#18B748"
            title="Editar información"
            action={() => navigation.navigate('Home')}
          />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingLeft: 23,
    paddingRight: 23,
    paddingTop: 50,
    paddingBottom: 50,
    alignSelf: 'stretch',
    textAlign: 'center',
  },
  containerImage: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  circleContainer: {
    borderWidth: 1,
    borderColor: '#0BBEDE',
    borderRadius: 50,
    height: 100,
    width: 100,
  },
  user: {
    height: 50,
    width: 50,
    marginLeft: 23,
    marginTop: 23,
  },
  title: {
    fontSize: 30,
    textAlign: 'center',
    color: '#fff',
  },
  text: {
    color: '#fff',
    paddingTop: 20,
  },
  input: {
    height: 40,
    fontSize: 13,
    padding: 4,
    borderColor: '#0BBEDE',
    borderBottomWidth: 1,
  },
});

export default MyAccount;
