import React, {Component} from 'react';
import AppButton from '../components/appButton';
import {
  TouchableHighlight,
  View,
  Text,
  ScrollView,
  StyleSheet,
  Image,
  Modal,
  Alert,
} from 'react-native';
import {Icon} from 'native-base';
import DatePicker from 'react-native-date-picker';
import {Header} from 'react-native-elements';


class PublicationDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      date: new Date(),
      time: '',
    };
  }
  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  onCancel() {
    this.TimePicker.close();
  }

  onConfirm(hour, minute) {
    this.setState({time: `${hour}:${minute}`});
    this.TimePicker.close();
  }

  render() {
    const { navigation } = this.props;
    const image = navigation.state.params.publication.images[0]    
    return (
      <ScrollView>
        <Header
          leftComponent={
            <Icon
              name="arrow-back"
              style={{color: '#fff'}}
              onPress={() => navigation.navigate('Home')}
            />
          }
          containerStyle={{
            backgroundColor: '#3B5998',
            height: 80,
            marginTop:-20,
            borderColor:'#3B5999',
          }}
        />
        <View style={styles.containerImage}>
          <Image
            source={image}
            style={styles.image}
            onPress={() => navigation.navigate('Home')}
          />
        </View>
        <View style={styles.containerInfo}>
          <Text style={styles.titleService}>{navigation.state.params.publication.title}</Text>
          <Text>
            {navigation.state.params.publication.description}
          </Text>
          <Text style={styles.company}>
            Company:
          </Text>
          <Text style={styles.companyTitle}>
            {navigation.state.params.publication.company.nombre}
          </Text>
          <AppButton
            bgColor="#18B748"
            title="Reservar servicio"
            action={() => this.setModalVisible(true)}
          />
          <Modal
            animationType="slide"
            transparent={false}
            visible={this.state.modalVisible}
            onRequestClose={() => {
              Alert.alert('Modal has been closed.');
            }}>
            <View style={styles.mainContainerModal}>
              <View style={styles.containerModal}>
                <TouchableHighlight
                  onPress={() => {
                    this.setModalVisible(!this.state.modalVisible);
                  }}
                  style={styles.iconContent}>
                  <Text>
                    <Icon name="close" size={24} color="black" />
                  </Text>
                </TouchableHighlight>
                <Text style={styles.titleModal}>
                  Reservar fecha y hora del servicio
                </Text>
                <View>
                <DatePicker
                    date={this.state.date}
                    onDateChange={date => this.setState({ date })}
                  />
                </View>
                <AppButton
                  bgColor="#18B748"
                  title="Resevar una cita"
                  action={() => this.setModalVisible(false)}
                />
              </View>
            </View>
          </Modal>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  mainContainerModal: {
    backgroundColor: '#EFECEC',
    height: 800,
  },
  containerModal: {
    alignSelf: 'stretch',
    textAlign: 'center',
    padding: 20,
    margin: 20,
  },
  iconContent: {
    marginLeft: 'auto',
  },
  containerImage: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleModal: {
    fontSize: 25,
    padding: 20,
  },
  containerInfo: {
    textAlign: 'center',
    paddingLeft: 30,
    paddingRight: 30,
    paddingTop: 10,
  },
  image: {
    width: 250,
    height: 250,
    resizeMode: 'contain',
  },
  title: {
    fontSize: 15,
    width: 200,
    height: 160,
    padding: 20,
  },
  titleService: {
    fontSize: 19,
    textAlign: 'center',
    paddingBottom: 10
  },
  company: {
    fontSize: 20,
    paddingTop: 10,
  },
  companyTitle: {
    fontSize: 20,
    fontWeight: 'bold',
    paddingTop: 10,
  },
  description: {
    fontSize: 13,
    width: 200,
    height: 160,
    padding: 20,
  },
  vendorContainer: {
    alignSelf: 'stretch',
    paddingRight: 40,
  },
  vendor: {
    paddingTop: 20,
    width: 300,
    height: 300,
  },
});

export default PublicationDetail;
