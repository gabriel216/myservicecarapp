import React, {Component} from 'react';
import {
  TouchableHighlight,
  View,
  Text,
  StyleSheet,
  Image,
  ScrollView,
} from 'react-native';
import {Icon} from 'native-base';
import { ServiceConsumer } from '../services/context';

class Category extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {navigation, title, logo} = this.props;
    return (
      <ServiceConsumer>
        {value => {
          return(
            <TouchableHighlight onPress={() => 
            {
              value.filterByType(title),
              navigation.navigate('CategoryDetail', {title:title})
            }}
              style={styles.containerInfo}>
              <View style={styles.containerInfo}>
                <Image source={logo} style={styles.image} />
                <Text style={styles.title}>{title} {'\n'}</Text>
              </View>
            </TouchableHighlight>
          )
        }
        }
      </ServiceConsumer>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignSelf: 'stretch',
    marginTop: 10,
    marginLeft: 30,
    marginRight: 30,
    height: 150,
  },
  containerInfo: {
    flexDirection: 'row',
    borderColor: '#EFECEC',
    padding:10,
  },
  image: {
    marginTop: 0,
    marginRight:15,
    width: 100,
    height: 90,
    resizeMode: 'contain',
  },
  title: {
    fontSize: 17,
    color: '#545454',
    fontWeight: 'bold',
    marginRight: 15,
    paddingTop: 10,
  },
  description: {
    fontSize: 13,
    width: 200,
    height: 160,
    padding: 20,
  },
});

export default Category;
