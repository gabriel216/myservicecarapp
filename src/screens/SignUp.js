import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  Image,
  ScrollView,
} from 'react-native';
import AppButton from '../components/appButton';
import {Icon} from 'native-base';
import Logo from '../../assets/images/auto-logo.png';
import {Header} from 'react-native-elements';

class SignUp extends Component {
  render() {
    const {navigation} = this.props;
    return (
      <ScrollView style={styles.container}>
        <Header
          leftComponent={
            <Icon
              name="arrow-back"
              style={{color: '#fff'}}
              onPress={() => navigation.goBack()}
            />
          }
          containerStyle={{
            backgroundColor: '#3B5998',
            height: 60,
            borderColor:'#3B5999',
          }}
        />
        <Text style={styles.title}>Service Car App</Text>
        <View style={styles.contentLogo}>
          <Image source={Logo} style={styles.logo} />
        </View>
        <View style={styles.containerInfo}>
          {/* Nombre  */}
          <Text style={styles.text}>Nombre</Text>
          <TextInput
            placeholder="Nombre"
            style={styles.input}
            leftIcon={
              <Icon
                name="name"
                size={24}
                color="black"
                style={{paddingRight: 7}}
              />
            }
          />
          {/* Apellido  */}
          <Text style={styles.text}>Apellido</Text>
          <TextInput
            placeholder="Apellido"
            style={styles.input}
            leftIcon={
              <Icon
                name="lastname"
                size={24}
                color="black"
                style={{paddingRight: 7}}
              />
            }
          />
          {/* Correo  */}
          <Text style={styles.text}>Correo electrónico</Text>
          <TextInput
            placeholder="Correo"
            style={styles.input}
            leftIcon={
              <Icon
                name="mail"
                size={24}
                color="black"
                style={{paddingRight: 7}}
              />
            }
          />
          {/* Contraseña */}
          <Text style={styles.text}>Contraseña</Text>
          <TextInput
            placeholder="Contraseña"
            type="password"
            style={styles.input}
            leftIcon={
              <Icon
                name="password"
                size={24}
                color="black"
                style={{paddingRight: 7}}
              />
            }
          />
          {/* Confirmar contraseña */}
          <Text style={styles.text}>Confirmar contraseña</Text>
          <TextInput
            placeholder="Confirmar contraseña"
            type="Password"
            style={styles.input}
            leftIcon={
              <Icon
                name="mail"
                size={24}
                color="black"
                style={{paddingRight: 7}}
              />
            }
          />
          <AppButton
            bgColor="#18B748"
            title="Registrarse"
            action={() => navigation.navigate('Home')}
          />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#3B5998',
  },
  containerInfo: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 10,
    alignSelf: 'stretch',
    textAlign: 'center',
    backgroundColor: '#3B5998',
    height: 600,
  },
  contentLogo: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    width: 150,
    height: 150,
  },
  title: {
    fontSize: 30,
    textAlign: 'center',
    color: '#fff',
    marginTop: 20,
  },
  text: {
    color: '#fff',
    paddingTop: 20,
  },
  input: {
    height: 40,
    fontSize: 13,
    padding: 4,
    color: '#fff',
    borderColor: '#0BBEDE',
    borderBottomWidth: 1,
  },
});

export default SignUp;
