import React, {Component} from 'react';
import AppButton from '../components/appButton';
import {
  TouchableHighlight,
  View,
  Text,
  ScrollView,
  StyleSheet,
  Image,
  Modal,
  Alert,
} from 'react-native';
import {Icon} from 'native-base';
import { ServiceConsumer } from '../services/context';
import Publication from './Publication';
import {Header} from 'react-native-elements';


class CategoryDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      date: new Date(),
      time: '',
    };
  }
  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  onCancel() {
    this.TimePicker.close();
  }

  onConfirm(hour, minute) {
    this.setState({time: `${hour}:${minute}`});
    this.TimePicker.close();
  }

  render() {
    const {navigation} = this.props;
    return (
      <ScrollView>
        <Header
          leftComponent={
            <Icon
              name="arrow-back"
              style={{color: '#fff', margin:10}}
              onPress={() => navigation.navigate('Categories')}
            />
          }
          containerStyle={{
            backgroundColor: '#3B5998',
            height: 80,
            marginTop:-20,
            borderColor:'#3B5999',
          }}
        />
        <Text style={styles.title}>{navigation.state.params.title}</Text>
        <ServiceConsumer>
          {value => {
            return ( value.publicationsFilter.length>0 ? 
              value.publicationsFilter.map(publication => {
                return (
                  <Publication
                    key={publication.id}
                    publication={publication}
                    navigation={navigation}
                  /> 
                );
              })
              :
                <Text style={styles.title}>
                  Oops.. aún no hay servicios en esta categoría
                </Text>
            )
          }}
        </ServiceConsumer>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  mainContainerModal: {
    backgroundColor: '#EFECEC',
    height: 800,
  },
  containerModal: {
    alignSelf: 'stretch',
    textAlign: 'center',
    padding: 20,
    margin: 20,
  },
  iconContent: {
    marginLeft: 'auto',
  },
  containerImage: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleModal: {
    fontSize: 25,
    padding: 20,
  },
  containerInfo: {
    textAlign: 'center',
    paddingLeft: 30,
    paddingRight: 30,
    paddingTop: 10,
  },
  image: {
    width: 250,
    height: 250,
    resizeMode: 'contain',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    width: 380,
    padding: 10,
    textAlign: 'center',
  },
  description: {
    fontSize: 13,
    width: 200,
    height: 160,
    padding: 20,
  },
  vendorContainer: {
    alignSelf: 'stretch',
    paddingRight: 40,
  },
  vendor: {
    paddingTop: 20,
    width: 300,
    height: 300,
  },
});

export default CategoryDetail;
