import React, {Component} from 'react';
import {StyleSheet, ScrollView, View, SafeAreaView, Text } from 'react-native';
import HeaderMenu from '../components/MenuButton';
import Category from './Category';
import Lavado from '../../assets/images/lavado-de-coches.png';
import Mecanica from '../../assets/images/mecanica.png';
import Advertencia from '../../assets/images/advertencia.png';

class Categories extends Component {
  constructor(props) {
    super(props);
  }


  render() {
    const {navigation} = this.props;
    return (
      <ScrollView>
        <HeaderMenu navigation={navigation} />
        <Category title={'Servicio de autolavado'} logo={Lavado} navigation={navigation}/>
        <Category title={'Servicio de mecánica'} logo={Mecanica} navigation={navigation}/>
        <Category title={'Servicio de grúa'} logo={Advertencia} navigation={navigation}/>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
        flex: 1,
        marginTop: 10,
        paddingLeft: 30,
        paddingRight: 30,
        textAlign: 'center',
  },
  containerCarousel: {
    flex: 1,
    textAlign: 'center',
},
  title: {
      textAlign: 'center',
      fontSize: 18,
      fontWeight: 'bold',
      fontFamily: 'Arial',
  }
});

export default Categories;
