import React, {Component} from 'react';
import {
  TouchableHighlight,
  View,
  Text,
  StyleSheet,
  Button,
  Image,
} from 'react-native';
import Profile from '../../assets/images/carWash.jpg';

class Publication extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { navigation, publication } = this.props;
    const image = publication.images[0]
    return (
      <TouchableHighlight
        style={styles.container}
        onPress={() => navigation.navigate('PublicationDetail', {publication:publication})}>
        <View style={styles.containerInfo}>
          <Image source={image} style={styles.image} />
          <Text style={styles.title}>{publication.title}</Text>
        </View>
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignSelf: 'stretch',
    marginLeft: 30,
    marginRight: 30,
    height: 150,
    width: 300,
    paddingRight:15,
    paddingLeft:15,
  },
  containerInfo: {
    flexDirection: 'row',
  },
  image: {
    marginTop: 0,
    width: 120,
    height: 120,
    resizeMode: 'contain',
  },
  title: {
    fontSize: 17,
    fontWeight: 'bold',
    color: '#545454',
    width: 180,
    height: 160,
    padding: 20,
  },
  description: {
    fontSize: 13,
    width: 200,
    height: 160,
    padding: 20,
  },
});

export default Publication;
