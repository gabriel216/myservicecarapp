import React, { Component } from 'react';
import { ScrollView, StyleSheet, View } from 'react-native';
import Publication from './Publication';
import HeaderMenu from '../components/MenuButton';
import { ServiceConsumer } from '../services/context';

class Home extends Component {
    constructor (props) {
        super(props);
    }
    render() {
        const { navigation } = this.props
        return( 
                <ScrollView style={{flex:1,textAlign: 'center',}}>
                    <HeaderMenu navigation={navigation}/>                
                    <View style={{flex:1,textAlign: 'center',}}>
                        <ServiceConsumer>
                            {value => {
                                return(
                                    value.publications.map( publication => {
                                        return <Publication
                                            key={publication.id}
                                            publication={publication}
                                            navigation={navigation}
                                        />
                                    })
                                )
                                }
                            }
                        </ServiceConsumer>
                    </View>
                </ScrollView>
        )
    }
}

export default Home;