import React, { Component } from 'react';
import { Header } from 'react-native-elements';
import { Left, Right, Icon } from 'native-base';
console.disableYellowBox = true;

export default class MenuButton extends Component {
    constructor(props) {
        super(props)
    }
    static navigationOptions = {
        drawerIcon: ({ tintColor }) => (
            <Icon name="list" style={{ fontSize: 24, color: tintColor }} />
        )
    }
    render() {
        const { navigation } = this.props;
        return (
            <Header
                leftComponent={<Icon name="menu" style={{color:'#fff'}} onPress={() => navigation.openDrawer()} />}
                centerComponent={{ text: 'My Car Service', style: { color: '#fff' } }}
                rightComponent={<Icon name="home" style={{color:'#fff'}} onPress={() => navigation.navigate('Home')}/>}
                containerStyle={{
                    backgroundColor: '#3B5998'
                }}
            />
        )
    }
}