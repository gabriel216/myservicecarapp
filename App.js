import React, {Component} from 'react';
import {StylesSheet, Text, View} from 'react-native';
import Navigator from './src/navigation/Navigator';
import {ServiceProvider} from './src/services/context';

export default class App extends Component {
  render() {
    return (
      <ServiceProvider>
        <Navigator />
      </ServiceProvider>
    );
  }
}
